import {Button, Card} from 'react-bootstrap'
import {useState} from 'react'
import {Link} from 'react-router-dom'


export default function CourseCard({courseProps}) {
    //Checks to see if the data was successfully passed
    console.log(courseProps)
     console.log(typeof courseProps)

    // Desctructuring the data to avoid dot notation
    const {name,description, price, _id} = courseProps;

    // 3 Hooks in react
    // 1. useState
    // 2. useEffect
    // 3. useContext

    // Use the useState hook for component to be able to store state
    // State are used to keep track of information related to individual components
    // Syntax -> const [getter,setter] = useState(initialGetterValue);

   
    const [count, setCount] =useState(0);
    const [seat, setSeat] =useState(30);
    //console.log(useState(0));

    /*function enroll() {
        if( seat === 0){
            alert("No more Seats Available")
        } else {
            setCount(count+1)
            setSeat(seat-1)
        }
        //console.log("Enrollees" + count)
    }*/

	return (
			<Card className="m-4">
                <Card.Body>
                    <Card.Title>{name}</Card.Title>
                    <Card.Subtitle>Description:</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text>{price}</Card.Text>
                    <Card.Text>{count} Enrollees </Card.Text>
                    <Card.Text>{seat} Slots Available </Card.Text>
                    {/*<Button onClick={enroll} variants="primary">Enroll</Button>*/}
                    <Link className="btn btn-primary" to={`/courseView/${_id}`}>Details</Link>
                </Card.Body>
             </Card>
		)
}