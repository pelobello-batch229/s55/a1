import {useContext} from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import {Link, NavLink, Outlet} from 'react-router-dom'
import UserContext from '../UserContext.js'


export default function AppNavbar(){

	// const [user,setUser] = useState(localStorage.getItem('email'))

	const {user} = useContext(UserContext);
	console.log(user);

	return(
		
		<Navbar bg="light" expand="lg" className="p-3" text="center">
			<Navbar.Brand as={NavLink} exact to="/">Zuitt</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id ="basic-navbar-nav">{/*SelfClosing*/}
				<Nav className="ml-auto">
					<Nav.Link as={NavLink} exact to="/">Home</Nav.Link>
					<Nav.Link as={NavLink} exact to="/courses">Courses</Nav.Link>
					{

						(user.token !== null)?
						<Nav.Link as={NavLink} exact to="/logout">Logout</Nav.Link>
						:
						<>
							<Nav.Link as={NavLink} exact to="/login">Login</Nav.Link>
							<Nav.Link as={NavLink} exact to="/register">Register</Nav.Link>
						</>
					}

				</Nav>
			</Navbar.Collapse>
			<Outlet/>
		</Navbar>
	




		)
}