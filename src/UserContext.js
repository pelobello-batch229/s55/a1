import React from 'react';

// Creates a Context Object
const UserContext = React.createContext();


// Provider Component// The "Provider" Component allows other components to consume/user the context object and supply the necessary information needed to the context object
export const UserProvider = UserContext.Provider;

export default UserContext;