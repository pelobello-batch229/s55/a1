import {Form, Button} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext.js'
import {Navigate,useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'
// use useEffect conditional limit // Like a function


export default function Register() {
	const navigate = useNavigate();
	const {user} = useContext(UserContext);

	// State Hooks -> store values of the inpute fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const[isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password1)
	console.log(password2)

	useEffect(() => {
		// Validation to enable register button when all fields are populated and both password match
		if((firstName !== '' && lastName !== '' && email !== '' && mobileNo !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	})

	// Function to simulate user registration

	function registerUser(event) {
		// prevents page redirection via form submission
		event.preventDefault();

		fetch('http://localhost:4000/users/checkEmail',{
			method: 'Post',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email

			})
		},[])
		.then(result => result.json())
		.then(data => {
				if(data !== true){
					fetch(`${process.env.REACT_APP_API_URL }/users/register`,{
						method: 'POST',
						headers: {
							'Content-Type': 'application/json'
						},
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							email: email,
							mobileNo: mobileNo,
							password: password1
						})
					},[])

					Swal.fire({
				    			title: "Registration Successfull",
				    			icon: "success",
				    			text: "Welcome to Zuitt"
		    		})
		    		navigate("/login")		
		    							
					setFirstName('')
					setLastName('')
					setEmail('');
					setMobileNo('');
					setPassword1('');
					setPassword2('');
					

					
				}else{
					Swal.fire({
				    			title: "Duplication email found",
				    			icon: "warning",
				    			text: "Please provide a different email"
		    		})
				}
		})


		
	}



	return(

		(user.token!== null)?
			<Navigate to="/courses"/>
		:
		    <Form onSubmit={event=> registerUser(event)} >
		    	<Form.Group className="mb-3" controlId="firstName">
		        	<Form.Label>First Name</Form.Label>
		        	<Form.Control type="text" placeholder="Enter first name"  value={firstName} onChange={event => setFirstName(event.target.value)} required/>
		      	</Form.Group>

		      	<Form.Group className="mb-3" controlId="lastName">
		        	<Form.Label>Last Name</Form.Label>
		        	<Form.Control type="text" placeholder="Enter last name"  value={lastName} onChange={event => setLastName(event.target.value)} required/>
		      	</Form.Group>

		      	<Form.Group className="mb-3" controlId="userEmail">
		        	<Form.Label>Email address</Form.Label>
		        	<Form.Control type="email" placeholder="Enter email"  value={email} onChange={event => setEmail(event.target.value)} required/>
		        	<Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        	</Form.Text>
		      	</Form.Group>

		      	<Form.Group className="mb-3" controlId="mobileNo">
		        	<Form.Label>Mobile Number</Form.Label>
		        	<Form.Control type="text" placeholder="Enter last name"  value={mobileNo} onChange={event => setMobileNo(event.target.value)} required/>
		      	</Form.Group>		      	

		      	<Form.Group className="mb-3" controlId="Password1">
		        	<Form.Label>Password</Form.Label>
		        	<Form.Control type="password" placeholder="Password" value={password1} onChange={event => setPassword1(event.target.value)} required/>
		      	</Form.Group>

		      	<Form.Group className="mb-3" controlId="Password2">
		        	<Form.Label>Verify Password</Form.Label>
		        	<Form.Control type="password" placeholder="Verify Password" value={password2} onChange={event => setPassword2(event.target.value)} required />
		      	</Form.Group>

		     {/*Conditional Rendering -> If active button is clickable -> inactive button is not clickable*/}
		     {
		     	(isActive)?	
		     	<Button variant="primary" type="submit" controlId="submitBtn" >Register</Button>	

		     	: 
		     	<Button variant="primary" type="submit" controlId="submitBtn" disabled>Register</Button>	
		     }
		      

		    </Form>
		)
}