import {Form, Button} from 'react-bootstrap';
// hook
import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom'
import UserContext from '../UserContext.js'
import Swal from 'sweetalert2'


export default function Login() {
	
	// Allow us to consume the User Context object and its properties
	const {user, setUser} = useContext(UserContext);



	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	// State to dertermine whether submit button is enabled or not
	const[isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password)

	//validation to enable submit button when all fields are populated and both password match
	useEffect(() => {
		
		if(email !== '' && password !== ''){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	}, [email, password])
	//dependency array



	function userLogin(event) {

		// Prevent pag to redirection via form submission
		event.preventDefault();

		// Process a fecth request to the corresponding API
		/*
			Syntax:
				fetch('url', {options})"Option is optional//reques method"
				.then(res => res.json())
				.then(data => {})
		*/
		fetch(`${process.env.REACT_APP_API_URL }/users/login`,{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(result => result.json())
		.then(data =>{
			console.log(data)

		// If no user information is found, the "access" property will not be available and will return undefined
    	// Using the typeof operator will return a string of the data type of the variable/expression it preceeds which is why the value being compared is in a string data type
			if(typeof data.access !== "undefined"){
    		localStorage.setItem('token', data.access)
    		retrieveUserDetails(data.access);

	    		Swal.fire({
	    			title: "Login Successfull",
	    			icon: "success",
	    			text: "Welcome to Zuitt"
	    		})
    		}else {
    			Swal.fire({
	    			title: "Authentication Failed",
	    			icon: "error",
	    			text: "Check your login details and try again!"
	    		})
    		}
		})
///////////////////////////////////////////////////////////
		// Set the email of the authenticated user in the local storage
		/*
			Syntax:
				localStorage.setItem('propertyName', value)
		*/
		// setItem saving to the localstorage

		/*localStorage.setItem('email', email);

		setUser({
			// getItem getting the item in the localstorage
			email: localStorage.getItem('email')
		})*/

		setEmail('');
		setPassword('');
		
	}

//////////////////////////////////////////////////////////
	const retrieveUserDetails = (token) =>{
		// The token will be sent as part of the request's header information
    	// We put "Bearer" in front of the token to follow implementation standards for JWTs

    	fetch(`${process.env.REACT_APP_API_URL }/users/details`, {
    		headers:{
    			Authorization: `Bearer ${token}`
    		}
    	})
    	.then(res => res.json())
    	.then(data =>{
    		console.log(data);

    		// Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
    		setUser({
    			token: localStorage.getItem('token'),
    			id: data._id,
    			isAdmin: data.isAdmin
    		})
    		console.log(user)
    	})


	}

	return(
		
		(user.token!== null)?
			<Navigate to="/courses"/>
		:


		    <Form onSubmit={event=> userLogin(event)} >
		    	<h2>Login</h2>
		      <Form.Group className="mb-3" controlid="userEmail">
		        <Form.Label>Email</Form.Label>
			        <Form.Control 
			        type="email" 
			        placeholder="Enter email"  
			        value={email} 
			        onChange={event => setEmail(event.target.value)} 
			        required
			        />
		        <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        </Form.Text>
		      </Form.Group>

		      <Form.Group className="mb-3" controlid="Password">
		        <Form.Label>Password</Form.Label>
		        <Form.Control type="password" placeholder="Password" value={password} onChange={event => setPassword(event.target.value)} required/>
		      </Form.Group>

		     {
		     	isActive?	
		     	<Button variant="success" type="submit" controlid="submitBtn" >Login</Button>	
		     	: 
		     	<Button variant="success" type="submit" controlid="submitBtn" disabled>Login</Button>	
		     }
		      

		    </Form>
		)
}