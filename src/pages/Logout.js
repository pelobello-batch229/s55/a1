//redirect 
import{useContext, useEffect} from 'react'
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext.js'
export default  function Logout(){


	// Consume the UserContext object and destructure it to access the user state and unsetUser funtion ferom the context provider
	const {unsetUser, setUser} = useContext(UserContext);

	//Clear the localStorage of the user's information//unsetUser is a function from/createdin the App.js stored in the UserContext.js
	unsetUser();

	useEffect(() => {

		//Set the user state back to tits original
		setUser({token:null,id:null,isActive: null})
	})



	return(
			<Navigate to="/login"/>

		)
}